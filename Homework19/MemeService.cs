﻿using Microsoft.Extensions.Hosting;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Homework19.Services;

namespace Homework19
{
    public class MemeService : IHostedService
    {
        private readonly IMemeProvider _provider;

        public MemeService(IMemeProvider provider)
        {
            _provider = provider;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            Console.WriteLine("Please type full path to picture for motivator meme.");
            var path = Console.ReadLine();
            while (!FileExists(path))
            {
                Console.WriteLine("No such file. Try again. Full path required.");
                path = Console.ReadLine();
            }
            Console.WriteLine("Please type the text for motivator meme.");
            var text = Console.ReadLine();
            var result = _provider.CreateMeme(path, text);
            Console.WriteLine($"Ready. Your meme is here: {result}.");
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _provider?.Dispose();
            return Task.CompletedTask;
        }

        private bool FileExists(string path) => File.Exists(path);
    }
}
