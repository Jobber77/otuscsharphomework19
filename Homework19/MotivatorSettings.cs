﻿namespace Homework19
{
    public class MotivatorSettings
    {
        public string OutputFileName { get; set; }
        public int PictureWidth { get; set; }
        public int PictureHeight { get; set; }
        public int Padding { get; set; }
        public int TextAreaHeight { get; set; }
    }
}
