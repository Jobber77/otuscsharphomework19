﻿namespace Homework19.Services
{
    public class MotivatorDirector : IMemeDirector
    {
        public void PrepareMeme(IMemeBuilder builder, string picturePath, string text)
        {
            builder.Reset();
            builder.AddBackgroundColor();
            builder.AddPicture(picturePath);
            builder.AddPictureBorder();
            builder.AddText(text);
        }
    }
}
