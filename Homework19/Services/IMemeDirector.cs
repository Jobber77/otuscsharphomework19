﻿namespace Homework19.Services
{
    public interface IMemeDirector
    {
        public void PrepareMeme(IMemeBuilder builder, string sourceImagePath, string text);
    }
}
