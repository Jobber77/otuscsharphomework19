﻿namespace Homework19.Services
{
    public class MotivatorProvider : IMemeProvider
    {
        private readonly IMemeDirector _memeDirector;
        private readonly IMemeBuilder _builder;

        public MotivatorProvider(IMemeDirector memeDirector, IMemeBuilder builder)
        {
            _memeDirector = memeDirector;
            _builder = builder;
        }

        public string CreateMeme(string sourceImagePath, string message)
        {
            _memeDirector.PrepareMeme(_builder, sourceImagePath, message);
            return _builder.Build();
        }

        public void Dispose()
        {
            _builder?.Dispose();
        }
    }
}
