﻿using System;

namespace Homework19.Services
{
    public interface IMemeBuilder : IBuilder<string>, IDisposable
    {
        public void Reset();
        public void AddBackgroundColor();
        public void AddPicture(string picturePath);
        public void AddPictureBorder();
        public void AddText(string text);
    }
}
