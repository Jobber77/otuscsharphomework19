﻿using System;
using System.IO;
using Microsoft.Extensions.Options;
using SixLabors.Fonts;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using SixLabors.Primitives;
using SixLabors.Shapes;

namespace Homework19.Services
{
    public class MotivatorBuilder : IMemeBuilder
    {
        private readonly MotivatorSettings _settings;
        protected Image<Rgba32> Background { get; set; }

        public MotivatorBuilder(IOptionsSnapshot<MotivatorSettings> settings)
        {
            _settings = settings.Value;
            Reset();
        }
        
        public void Reset()
        {
            var (width, height) = CalculateBackgroundSize();
            Background = new Image<Rgba32>(width, height);
        }

        public void AddBackgroundColor() => Background.Mutate(context => context.BackgroundColor(Color.SkyBlue));

        public void AddPicture(string picturePath)
        {
            using var image = Image.Load(picturePath);
            using var copyImage = image.Clone(context => 
                context.Resize(new ResizeOptions
                {
                    Mode = ResizeMode.Crop, 
                    Size = new Size { Height = _settings.PictureHeight, Width = _settings.PictureWidth}
                }));
            Background.Mutate(context => 
                context.DrawImage(copyImage, new Point { X = _settings.Padding, Y = _settings.Padding }, 1));
        }

        public void AddPictureBorder()
        {
            Background.Mutate(context =>
            {
                var rect = new RectangularPolygon(_settings.Padding, _settings.Padding, _settings.PictureWidth, _settings.PictureHeight);
                var path = new PathCollection(rect);
                context.Draw(Color.White, 5, path);
            });
        }

        public void AddText(string text)
        {
            Background.Mutate(context =>
            {
                var imageSize = context.GetCurrentSize();
                var textAreaSize = new Size { Width = _settings.PictureWidth, Height = _settings.TextAreaHeight };
                var font = SystemFonts.CreateFont("Arial", 39, FontStyle.Regular);
                var expectedSize = TextMeasurer.Measure(text, new RendererOptions(font));
                float scalingFactor = Math.Min(textAreaSize.Width / expectedSize.Width, textAreaSize.Height / expectedSize.Height);
                Font scaledFont = new Font(font, scalingFactor * font.Size);
                
                var center = new PointF(imageSize.Width / 2, imageSize.Height - _settings.Padding - (_settings.TextAreaHeight / 2));
                
                var textGraphicsOptions = new TextGraphicsOptions(true)
                {
                    HorizontalAlignment = HorizontalAlignment.Center,
                    VerticalAlignment = VerticalAlignment.Center
                };
                context.DrawText(textGraphicsOptions, text, scaledFont, Color.White, center);
            });
        }

        public string Build()
        {
            Background.Save(_settings.OutputFileName);
            return System.IO.Path.Combine(Directory.GetCurrentDirectory(), _settings.OutputFileName);
        }

        private (int width, int height) CalculateBackgroundSize()
        {
            var width = _settings.PictureWidth + 2 * _settings.Padding;
            var height = _settings.PictureHeight + 2 * _settings.Padding + _settings.TextAreaHeight;
            return (width, height);
        }

        public void Dispose() => Background?.Dispose();
    }
}
