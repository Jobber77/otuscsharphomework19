﻿using System;

namespace Homework19.Services
{
    public interface IMemeProvider : IDisposable
    {
        string CreateMeme(string sourceImagePath, string message);
    }
}
