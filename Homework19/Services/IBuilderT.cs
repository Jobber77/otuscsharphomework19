﻿namespace Homework19.Services
{
    public interface IBuilder<T>
    {
        public T Build();
    }
}
