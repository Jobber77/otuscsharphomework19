﻿using Homework19.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Homework19
{
    class Program
    {
        static void Main(string[] args)
        {
            BuildHost().Run();
        }

        private static IHost BuildHost()
        {
            var host = new HostBuilder()
                .ConfigureAppConfiguration((context, config) =>
                {
                    config.AddJsonFile("appsettings.json", optional: false);
                })
                .ConfigureServices((context, services) =>
                {
                    services.Configure<MotivatorSettings>(context.Configuration.GetSection("MotivatorSettings"));
                    services.AddTransient<IMemeBuilder, MotivatorBuilder>();
                    services.AddTransient<IMemeDirector, MotivatorDirector>();
                    services.AddTransient<IMemeProvider, MotivatorProvider>();
                    services.AddHostedService<MemeService>();
                })
                .Build();
            return host;
        }
    }
}
